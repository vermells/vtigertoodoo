# -*- encoding: utf-8 -*-
###MODUL VTIGER###

from openerp.osv import osv, fields
from WSClient import WSClient


  
  
class import_data(osv.osv):
  
  _name = 'import.data'
  
  _columns = {
    'name' : fields.char('nom',size=60, required=True),
    'service_url' : fields.char('url',size=150, required=False, help='Url a la que conectamos'),
    'user' : fields.char('usuario', size=60, required=True, help='usuario'),
    'key' : fields.char('key loggin', size=100, required=True, help='llave del usuario'),
    
  }
  
  
  def obtener_id_company (self, cr, uid, client, contact, context=None):
    '''metodo para obtner id de la compañia'''
    id_cuenta= client.do_query('select account_no, accountname from Accounts;')
    obj= self.pool.get("res.company")
    
    
    name=''
    id_company_for_partner=0
    
    for idcuenta in range(len(id_cuenta)):
      
	if contact['account_id']==id_cuenta[idcuenta]['id']:
	  name=id_cuenta[idcuenta]['accountname']
	  print "NOMBRE: ", name
	  ids=obj.search(cr,uid,[('name','=',name)])
	  id_company_for_partner = ids[0]
	  
	
	  
    print "ID PARA GRABAR: " ,id_company_for_partner	  
    return id_company_for_partner
  
  
  
  
    
  def obtener_id_pais(self, cr, uid, contact, context=None):
    '''metodo para obtener id del pais'''
    valores_res_country={}
    
    
    
    nombre_vtiger=''
    
    obj= self.pool.get("res.country")
    
    if contact['mailingcountry']!='':
      nombre_vtiger=contact['mailingcountry']
      
      valores_res_country['name']=nombre_vtiger
      ids=obj.search(cr,uid, [('name','=',nombre_vtiger)])
      pais_actual=obj.browse(cr,uid,ids,context=None)
      if pais_actual.name!=nombre_vtiger:
	print "Creando res_country"
	obj.create(cr,uid,valores_res_country,context=None)
	
        
    
    ids = obj.search(cr, uid, [('name','=',nombre_vtiger)])
    if len(ids)>0:
      
      return ids[0]
    else:
      
      return None
    
  
  
  
  
  def obtener_id_estado(self, cr, uid, contact, context=None): 
    '''metodo para obtener id del state,  para que funcione tienen que estar los datos de pais
      guardados en vtiger,no pueden estar en blanco'''
    valores_res_country_state={}
    nombre_vtiger_state=''
    pais=''
    
    obj_country_state= self.pool.get("res.country.state")
    obj= self.pool.get("res.country")
    
    
    '''Si mailingstate te valor guardem el state i omplim els valors pero al 
       code i nom per guardar a res.country.state.
       
       Despres si mailingcountry te valor busquem el valor a res.country amb la funcio
       search i si troba un id el guardem
    '''
    if contact['mailingstate']!='':
      nombre_vtiger_state=contact['mailingstate']
      valores_res_country_state['code']=nombre_vtiger_state[:3]
      valores_res_country_state['name']=nombre_vtiger_state
      
    
      if contact['mailingcountry']!='':
	pais=contact['mailingcountry']
	ids = obj.search(cr, uid, [('name','=',pais)])
	if len(ids)>0:
	  valores_res_country_state['country_id']=ids[0]
	  
      else:
	ids = obj.search(cr,uid, [('name','=','España')])
	if len(ids)>0:
	  valores_res_country_state['country_id']=ids[0]
	else:
	  print "Valor España no disponible en tabla res.country"
      
      
      '''Aqui miramos si la provincia que introducimos ya existe para no duplicarla'''
      ids_prov=obj_country_state.search(cr,uid, [('name','=',nombre_vtiger_state)])
      provincia_actual=obj_country_state.browse(cr,uid,ids_prov,context=None)
      if provincia_actual.name!=nombre_vtiger_state:
	obj_country_state.create(cr,uid,valores_res_country_state,context=None)
      
    
    
    ids = obj_country_state.search(cr, uid, [('name','=',nombre_vtiger_state)])
    
    if len(ids)>0:
      
      return ids[0]
    else:
      return None
      
      
      
      
      
  def traspasar_cuentas(self,cr,uid, client, context=None):
    
    cuentas= client.do_query('select accountname from Accounts;')

    
    print cuentas
    
    obj_company = self.pool.get("res.company")
    valores_res_company={}
    for cuenta in range(len(cuentas)):
            
       
      #datos para grabar en res_company
      
      valores_res_company={}
      #valores_res_company['email']=cuentas[cuenta]['email1']
      #valores_res_company['phone']=cuentas[cuenta]['phone']
      valores_res_company['name']=cuentas[cuenta]['accountname']
      
      
      
      obj_company.create(cr, uid, valores_res_company,context=None)
      
    
  def traspasar_contactos(self, cr, uid, client, context=None):
    
        
    contactos=client.do_query('select account_id,firstname,lastname,\
			       email,phone,mobile,title,fax,reference,\
			       mailingcountry, mailingstate, mailingcity,\
			       mailingstreet, mailingzip from Contacts;')
    
    valores_res_partner={}
    obj_partner = self.pool.get("res.partner")
    for contacto in contactos:
      
      
      valores_res_partner['name']=contacto['firstname']+ ' ' +contacto['lastname']
      valores_res_partner['company_id']=int(self.obtener_id_company(cr, uid, client, contacto, context=None))
      valores_res_partner['email']=contacto['email']
      valores_res_partner['phone']=contacto['phone']
      valores_res_partner['mobile']=contacto['mobile']
      valores_res_partner['function']=contacto['title']
      valores_res_partner['fax']=contacto['fax']
      valores_res_partner['ref']=contacto['reference']
      valores_res_partner['country_id']= self.obtener_id_pais(cr,uid,contacto, context=None)
      valores_res_partner['state_id']=self.obtener_id_estado(cr,uid,contacto, context=None)
      valores_res_partner['city']=contacto['mailingcity']
      valores_res_partner['street']=contacto['mailingstreet']
      valores_res_partner['zip']=contacto['mailingzip']
      
      obj_partner.create(cr, uid, valores_res_partner, context=None)
  
    '''Guardamos los id de lo que sean compañias en res.partner
       Si es una compañia,guarda los datos de direccion  y actualiza los datos
       de las compañias en res.partner'''
    datos_cuentas=client.do_query('select accountname, phone,\
			       email1,website,fax,bill_city,\
			       bill_code, bill_country, bill_state,\
			       bill_street from Accounts;') 
    
    
    #identificadores is_company=true in res_partner
    ids_in_partner=obj_partner.search(cr, uid, [('is_company','=', True)])
    
    #objetos con is_Company como true
    company_id_in_partner=obj_partner.browse(cr,uid,ids_in_partner,context=None)
    
       
    valores_company={}
    valores_res_partner={} 
    obj_companies=self.pool.get("res.company")
    for datos in datos_cuentas:
      valores_res_partner['city']=datos['bill_city']
      valores_res_partner['zip']=datos['bill_code']
      valores_res_partner['street']=datos['bill_street']
      valores_res_partner['email']=datos['email1']
      valores_res_partner['website']=datos['website']
      valores_res_partner['fax']=datos['fax']
      valores_res_partner['phone']=datos['phone']
      valores_company['phone']=datos['phone']
      valores_company['email']=datos['email1']
      
      for company in company_id_in_partner:
	if datos['accountname']==company.name:
	  identificador=company.id
	  obj_partner.write(cr,uid,identificador,valores_res_partner,context=None)
	  obj_companies.write(cr,uid,identificador,valores_company,context=None)
      
    
  def create (self, cr, uid, values, context=None):
    client = WSClient.WSClient(values['service_url'])
    login = client.do_login(values['user'], values['key'])
     

    if login:
	print 'Login Successful'
    else:
	print 'Login Failed!'
    
    
      
    self.traspasar_cuentas(cr, uid, client, context=None)  
    self.traspasar_contactos(cr , uid, client, context=None) 
    
      
import_data()
    
  
  