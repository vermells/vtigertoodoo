{
  "name": "vtiger",
  "version" : "0.1",
  "author" : "open services bcn",
  "website" : "http://www.openbcn.net",
  "category" : "Unknown",
  "description" : """
    Modulo que conecta con vtiger para extraer datos
    
  """,
  "depends" : ['base','product'],
  "init_xml" : [ ],
  "demo_xml" : [],
  "data" : ['vtiger_view.xml'],
  "installable" : True
}

